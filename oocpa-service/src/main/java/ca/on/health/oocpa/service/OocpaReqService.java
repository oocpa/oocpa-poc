package ca.on.health.oocpa.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.on.health.oocpa.data.Dao.OocpaReqDAO;
import ca.on.health.oocpa.data.Dao.ReqWIPDAO;
import ca.on.health.oocpa.data.entity.OOCPAReqEntity;
import ca.on.health.oocpa.data.entity.ReqWIPEntity;

@Service
public class OocpaReqService {
	@Autowired
	private OocpaReqDAO oocpaReqDao;
	
	@Autowired
	private ReqWIPDAO wipDao;
	
	public OOCPAReqEntity save(OOCPAReqEntity req)
	{
		return oocpaReqDao.saveOrUpdate(req);
	}
	 @Transactional 
	public ReqWIPEntity save(ReqWIPEntity req)
	{
		return wipDao.saveOrUpdate(req);
	}
	 
	 @Transactional 
	public ReqWIPEntity searchWIPReq(String id)
	{
		return wipDao.findById(id);
	}
	
	public OOCPAReqEntity searchRequest(String id)
	{
		return oocpaReqDao.findById(id);
	}
	
}

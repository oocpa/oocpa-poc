package ca.on.health.oocpa.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ca.on.health.oocpa.data.entity.ReqWIPEntity;

	@Repository
	public interface ReqWIPRepository extends CrudRepository<ReqWIPEntity, Long> {
		@Query("SELECT r FROM  ReqWIPEntity r WHERE r.id = ?1")
		public List<ReqWIPEntity> findWIPRequest(Long id);
	}

package ca.on.health.oocpa.data.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PATIENT_INFO")
public class PatientInfoEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	
	   @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "created_timestamp", nullable = false)
	    private Date created;

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "last_update_timestamp", nullable = false)
	    private Date updated;

	    @PrePersist
	    protected void onCreate() {
	    	updated = created = new Date();
	    }

	    @PreUpdate
	    protected void onUpdate() {
	    	updated = new Date();
	    }
	    private String firstName;
	    private String lastName;
	    private String healthNumber;
	    private String version;
	    private String gender;
	    private Date dateOfBirth;
	    private String legalSigner;
	    private String legalGuardianFN;
	    private String legalGuardianLN;
	   
	    @OneToOne(cascade = CascadeType.ALL)
	    @JoinColumn(name="id")
	    private Address mailngAddress;
	    private String phone;
	    public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getHealthNumber() {
			return healthNumber;
		}

		public void setHealthNumber(String healthNumber) {
			this.healthNumber = healthNumber;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public Date getDateOfBirth() {
			return dateOfBirth;
		}

		public void setDateOfBirth(Date dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}

		public String getLegalSigner() {
			return legalSigner;
		}

		public void setLegalSigner(String legalSigner) {
			this.legalSigner = legalSigner;
		}

		public String getLegalGuardianFN() {
			return legalGuardianFN;
		}

		public void setLegalGuardianFN(String legalGuardianFN) {
			this.legalGuardianFN = legalGuardianFN;
		}

		public String getLegalGuardianLN() {
			return legalGuardianLN;
		}

		public void setLegalGuardianLN(String legalGuardianLN) {
			this.legalGuardianLN = legalGuardianLN;
		}

		public Address getMailngAddress() {
			return mailngAddress;
		}

		public void setMailngAddress(Address mailngAddress) {
			this.mailngAddress = mailngAddress;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		
}

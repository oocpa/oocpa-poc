package ca.on.health.oocpa.data.Dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ca.on.health.oocpa.data.entity.OOCPAReqEntity;
import ca.on.health.oocpa.data.entity.ReqWIPEntity;
import ca.on.health.oocpa.data.repository.ReqWIPRepository;
@Repository
public class ReqWIPDAO {
	@Autowired
	private ReqWIPRepository wipRepository;
	
	private Logger logger = LoggerFactory.getLogger(ReqWIPDAO.class);

	public ReqWIPEntity saveOrUpdate(ReqWIPEntity req) {
		return wipRepository.save(req);
	}
	
	public ReqWIPEntity findById(String id) {
		List<ReqWIPEntity> requests=wipRepository.findWIPRequest(Long.parseLong(id));
		
		if( requests.isEmpty()) {
			return null;
		} else {
			return requests.get(0);
		}
	}
	
}

package ca.on.health.oocpa.data.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ORG_CHARCS")
public class OrgCharcsEntity {
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="CREATE_TMESTMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTmestmp;

	@Column(name="CREATE_OPR_CD")
	private String createOprCd;

	@Column(name="EFFECTIVE_DATE")
	@Temporal(TemporalType.DATE)
	private Date effectiveDate;

	@Column(name="ORG_FRMD_DT")
	@Temporal(TemporalType.DATE)
	private Date orgFrmdDt;

	@Column(name="END_DATE")
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Column(name="ORIGINAL_REGN_DT")
	@Temporal(TemporalType.DATE)
	private Date originalRegnDt;

	@Column(name="ORG_ENG_NM")
	private String orgEngNm;

	@Column(name="ORG_FR_NM")
	private String orgFrNm;

	@Column(name="REGISTERED_LEG_NM")
	private String registeredLegNm;

	@Column(name="REPLACED_TMESTMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date replacedTmestmp;

	@Column(name="STD_ABBRVN_TXT")
	private String stdAbbrvnTxt;

	@Column(name="LANGUAGE_CD")
	private String languageCd;

	@Column(name="PROV_ST_CD")
	private String provStCd;

	@Column(name="COUNTRY_CD")
	private String countryCd;

	@Column(name="STATUS_REAS_TYPCD")
	private String statusReasTypcd;

	@Column(name="ORG_SUB_CLN_CD")
	private String orgSubClnCd;

	@Column(name="LEGAL_BODY_TYPCD")
	private String legalBodyTypcd;

	@Column(name="CORPORATION_NUM")
	private String corporationNum;

	@Column(name="FISCAL_YR_END_MTH")
	private BigDecimal fiscalYrEndMth;

	@Column(name="FISCAL_YR_END_DY")
	private BigDecimal fiscalYrEndDy;

	@Column(name="INCORPORATION_DT")
	@Temporal(TemporalType.DATE)
	private Date incorporationDt;

	@Column(name="REGISTRATION_NUM")
	private String registrationNum;

	@Column(name="STATUS_CD")
	private String statusCd;

	@ManyToOne
	@JoinColumn(name="STAKEHOLDER_NUM")
	private StakeholderEntity stakeholderNum;

	/**
	 * Constructor for OrgCharcs.
	 */
	public OrgCharcsEntity() {
		super();
	}

	/**
	 * @return the createTmestmp
	 */
	public Date getCreateTmestmp() {
		return createTmestmp;
	}

	/**
	 * @param createTmestmp the createTmestmp to set
	 */
	public void setCreateTmestmp(Timestamp createTmestmp) {
		this.createTmestmp = createTmestmp;
	}

	/**
	 * @return the createOprCd
	 */
	public String getCreateOprCd() {
		return createOprCd;
	}

	/**
	 * @param createOprCd the createOprCd to set
	 */
	public void setCreateOprCd(String createOprCd) {
		this.createOprCd = createOprCd;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the orgFrmdDt
	 */
	public Date getOrgFrmdDt() {
		return orgFrmdDt;
	}

	/**
	 * @param orgFrmdDt the orgFrmdDt to set
	 */
	public void setOrgFrmdDt(Date orgFrmdDt) {
		this.orgFrmdDt = orgFrmdDt;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the originalRegnDt
	 */
	public Date getOriginalRegnDt() {
		return originalRegnDt;
	}

	/**
	 * @param originalRegnDt the originalRegnDt to set
	 */
	public void setOriginalRegnDt(Date originalRegnDt) {
		this.originalRegnDt = originalRegnDt;
	}

	/**
	 * @return the orgEngNm
	 */
	public String getOrgEngNm() {
		return orgEngNm;
	}

	/**
	 * @param orgEngNm the orgEngNm to set
	 */
	public void setOrgEngNm(String orgEngNm) {
		this.orgEngNm = orgEngNm;
	}

	/**
	 * @return the orgFrNm
	 */
	public String getOrgFrNm() {
		return orgFrNm;
	}

	/**
	 * @param orgFrNm the orgFrNm to set
	 */
	public void setOrgFrNm(String orgFrNm) {
		this.orgFrNm = orgFrNm;
	}

	/**
	 * @return the registeredLegNm
	 */
	public String getRegisteredLegNm() {
		return registeredLegNm;
	}

	/**
	 * @param registeredLegNm the registeredLegNm to set
	 */
	public void setRegisteredLegNm(String registeredLegNm) {
		this.registeredLegNm = registeredLegNm;
	}

	/**
	 * @return the replacedTmestmp
	 */
	public Date getReplacedTmestmp() {
		return replacedTmestmp;
	}

	/**
	 * @param replacedTmestmp the replacedTmestmp to set
	 */
	public void setReplacedTmestmp(Timestamp replacedTmestmp) {
		this.replacedTmestmp = replacedTmestmp;
	}

	/**
	 * @return the stdAbbrvnTxt
	 */
	public String getStdAbbrvnTxt() {
		return stdAbbrvnTxt;
	}

	/**
	 * @param stdAbbrvnTxt the stdAbbrvnTxt to set
	 */
	public void setStdAbbrvnTxt(String stdAbbrvnTxt) {
		this.stdAbbrvnTxt = stdAbbrvnTxt;
	}

	/**
	 * @return the languageCd
	 */
	public String getLanguageCd() {
		return languageCd;
	}

	/**
	 * @param languageCd the languageCd to set
	 */
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}

	/**
	 * @return the provStCd
	 */
	public String getProvStCd() {
		return provStCd;
	}

	/**
	 * @param provStCd the provStCd to set
	 */
	public void setProvStCd(String provStCd) {
		this.provStCd = provStCd;
	}

	/**
	 * @return the countryCd
	 */
	public String getCountryCd() {
		return countryCd;
	}

	/**
	 * @param countryCd the countryCd to set
	 */
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	/**
	 * @return the statusReasTypcd
	 */
	public String getStatusReasTypcd() {
		return statusReasTypcd;
	}

	/**
	 * @param statusReasTypcd the statusReasTypcd to set
	 */
	public void setStatusReasTypcd(String statusReasTypcd) {
		this.statusReasTypcd = statusReasTypcd;
	}

	/**
	 * @return the orgSubClnCd
	 */
	public String getOrgSubClnCd() {
		return orgSubClnCd;
	}

	/**
	 * @param orgSubClnCd the orgSubClnCd to set
	 */
	public void setOrgSubClnCd(String orgSubClnCd) {
		this.orgSubClnCd = orgSubClnCd;
	}

	/**
	 * @return the legalBodyTypcd
	 */
	public String getLegalBodyTypcd() {
		return legalBodyTypcd;
	}

	/**
	 * @param legalBodyTypcd the legalBodyTypcd to set
	 */
	public void setLegalBodyTypcd(String legalBodyTypcd) {
		this.legalBodyTypcd = legalBodyTypcd;
	}

	/**
	 * @return the corporationNum
	 */
	public String getCorporationNum() {
		return corporationNum;
	}

	/**
	 * @param corporationNum the corporationNum to set
	 */
	public void setCorporationNum(String corporationNum) {
		this.corporationNum = corporationNum;
	}

	/**
	 * @return the fiscalYrEndMth
	 */
	public BigDecimal getFiscalYrEndMth() {
		return fiscalYrEndMth;
	}

	/**
	 * @param fiscalYrEndMth the fiscalYrEndMth to set
	 */
	public void setFiscalYrEndMth(BigDecimal fiscalYrEndMth) {
		this.fiscalYrEndMth = fiscalYrEndMth;
	}

	/**
	 * @return the fiscalYrEndDy
	 */
	public BigDecimal getFiscalYrEndDy() {
		return fiscalYrEndDy;
	}

	/**
	 * @param fiscalYrEndDy the fiscalYrEndDy to set
	 */
	public void setFiscalYrEndDy(BigDecimal fiscalYrEndDy) {
		this.fiscalYrEndDy = fiscalYrEndDy;
	}

	/**
	 * @return the incorporationDt
	 */
	public Date getIncorporationDt() {
		return incorporationDt;
	}

	/**
	 * @param incorporationDt the incorporationDt to set
	 */
	public void setIncorporationDt(Date incorporationDt) {
		this.incorporationDt = incorporationDt;
	}

	/**
	 * @return the registrationNum
	 */
	public String getRegistrationNum() {
		return registrationNum;
	}

	/**
	 * @param registrationNum the registrationNum to set
	 */
	public void setRegistrationNum(String registrationNum) {
		this.registrationNum = registrationNum;
	}

	/**
	 * @return the statusCd
	 */
	public String getStatusCd() {
		return statusCd;
	}

	/**
	 * @param statusCd the statusCd to set
	 */
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	/**
	 * @return the stakeholderNum
	 */
	public StakeholderEntity getStakeholderNum() {
		return stakeholderNum;
	}

	/**
	 * @param stakeholderNum the stakeholderNum to set
	 */
	public void setStakeholderNum(StakeholderEntity stakeholderNum) {
		this.stakeholderNum = stakeholderNum;
	}


}
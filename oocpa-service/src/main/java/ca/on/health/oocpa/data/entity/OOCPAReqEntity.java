package ca.on.health.oocpa.data.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name="OOCPA_REQUEST")
public class OOCPAReqEntity {

		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;

		private String treatment;
		private boolean workRelated;
		public String getTreatment() {
			return treatment;
		}
		
		   @Temporal(TemporalType.TIMESTAMP)
		    @Column(name = "created_timestamp", nullable = false)
		    private Date created;

		    @Temporal(TemporalType.TIMESTAMP)
		    @Column(name = "last_update_timestamp", nullable = false)
		    private Date updated;

		    @PrePersist
		    protected void onCreate() {
		    	updated = created = new Date();
		    }

		    @PreUpdate
		    protected void onUpdate() {
		    	updated = new Date();
		    }
		    @OneToOne(cascade = CascadeType.ALL)
		    @JoinColumn(name="patient_id")
		    private PatientInfoEntity patient;
		    @OneToOne(cascade = CascadeType.ALL)
		    @JoinColumn(name="physician_id")		    
		    private ReferingPhysicianEntity physician;
		    @OneToOne(cascade = CascadeType.ALL)
		    @JoinColumn(name="facility_id")
		    private OocpaFacilityEntity oocFacility;
		    

		public void setTreatment(String treatment) {
			this.treatment = treatment;
		}
		public boolean isWorkRelated() {
			return workRelated;
		}
		public void setWorkRelated(boolean workRelated) {
			this.workRelated = workRelated;
		}

		public PatientInfoEntity getPatient() {
			return patient;
		}

		public void setPatient(PatientInfoEntity patient) {
			this.patient = patient;
		}

		public ReferingPhysicianEntity getPhysician() {
			return physician;
		}

		public void setPhysician(ReferingPhysicianEntity physician) {
			this.physician = physician;
		}

		public OocpaFacilityEntity getFacility() {
			return oocFacility;
		}

		public void setFacility(OocpaFacilityEntity facility) {
			this.oocFacility = facility;
		}
		public Long getId() {
			return this.id;
		}
}

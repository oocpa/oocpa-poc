package ca.on.health.oocpa.data.Dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ca.on.health.oocpa.data.entity.CsnCharcsEntity;
import ca.on.health.oocpa.data.entity.StakeholderEntity;
import ca.on.health.oocpa.data.repository.StakeholderRepository;

@Repository
public class StakeholderDao {

	@Autowired
	private StakeholderRepository stakeholderRepository;
	 
	public StakeholderEntity findStakeholder(String sn) {
		
		List<StakeholderEntity> stakeholders = stakeholderRepository.findStakeholderBySN(sn);

		if( stakeholders.isEmpty()) {
			return null;
		} else {
			return stakeholders.get(0);
		}
	}
	
	public List<CsnCharcsEntity> findCSNsBySN(StakeholderEntity stakeholder){
		return null;
		
	}
	
	public boolean isOrgCharcsResultFound(StakeholderEntity stakeholder) {
		return false;
	}
}

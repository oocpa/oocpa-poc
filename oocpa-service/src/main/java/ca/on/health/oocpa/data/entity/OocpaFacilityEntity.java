package ca.on.health.oocpa.data.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="OOCPA_FACILITY_INFO")
public class OocpaFacilityEntity {
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;
		
	   @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "created_timestamp", nullable = false)
	    private Date created;

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "last_update_timestamp", nullable = false)
	    private Date updated;

	    @PrePersist
	    protected void onCreate() {
	    	updated = created = new Date();
	    }

	    @PreUpdate
	    protected void onUpdate() {
	    	updated = new Date();
	    }

		private String oocFacility;
	    
	    @OneToOne(cascade = CascadeType.ALL)
	    @JoinColumn(name="id")
	    private Address address;
	    private boolean oocPhysician;
	    private String firstName;
	    private String lastName;
	    private String phone;
	    private String fax;
	    private String email;

	    
	    public String getOocFacility() {
			return oocFacility;
		}

		public void setOocFacility(String facility) {
			this.oocFacility = facility;
		}

		public Address getAddress() {
			return address;
		}

		public void setAddress(Address address) {
			this.address = address;
		}

		public boolean isOocPhysician() {
			return oocPhysician;
		}

		public void setOocPhysician(boolean oocPhysician) {
			this.oocPhysician = oocPhysician;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getFax() {
			return fax;
		}

		public void setFax(String fax) {
			this.fax = fax;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}
	    
}

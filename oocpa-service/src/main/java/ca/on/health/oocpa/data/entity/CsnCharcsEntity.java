package ca.on.health.oocpa.data.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="@CSN_CHARCS")
public class CsnCharcsEntity {
	@Id
	@Column(name="CLAIM_SUB_NUM")
	private String claimSubNum;

	@Column(name="CLAIM_SUB_STAT_CD")
	private String claimSubStatCd;

	@Column(name="FIRST_INSD_SERV_DT")
	@Temporal(TemporalType.DATE)
	private Date firstInsdServDt;

	@Column(name="PRACTICE_PAY_TYPCD")
	private String practicePayTypcd;

	@Column(name="CREATE_OPR_CD")
	private String createOprCd;

	@Column(name="CREATE_TMESTMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTmestmp;

	@Column(name="EFFECTIVE_DATE")
	@Temporal(TemporalType.DATE)
	private Date effectiveDate;

	@Column(name="END_DATE")
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Column(name="REPLACED_TMESTMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date replacedTmestmp;

	@Column(name="BILLING_OPT_TYPCD")
	private String billingOptTypcd;

	@Column(name="RA_SEQ_TYPCD")
	private String raSeqTypcd;

	@Column(name="APPLICATION_DATE")
	@Temporal(TemporalType.DATE)
	private Date applicationDate;

	@Column(name="GROUP_TYPCD")
	private String groupTypcd;

	@Column(name="OCCS_NUM")
	private String occsNum;

	@Column(name="RA_RECIP_TYPCD")
	private String raRecipTypcd;

	@Column(name="RPLCTN_UNIQ_NUM")
	private BigDecimal rplctnUniqNum;


	/**
	 * Constructor for CsnCharcs.
	 */
	public CsnCharcsEntity() {
		super();
	}

	/**
	 * @return the claimSubNum
	 */
	public String getClaimSubNum() {
		return claimSubNum;
	}

	/**
	 * @param claimSubNum the claimSubNum to set
	 */
	public void setClaimSubNum(String claimSubNum) {
		this.claimSubNum = claimSubNum;
	}

	/**
	 * @return the claimSubStatCd
	 */
	public String getClaimSubStatCd() {
		return claimSubStatCd;
	}

	/**
	 * @param claimSubStatCd the claimSubStatCd to set
	 */
	public void setClaimSubStatCd(String claimSubStatCd) {
		this.claimSubStatCd = claimSubStatCd;
	}

	/**
	 * @return the firstInsdServDt
	 */
	public Date getFirstInsdServDt() {
		return firstInsdServDt;
	}

	/**
	 * @param firstInsdServDt the firstInsdServDt to set
	 */
	public void setFirstInsdServDt(Date firstInsdServDt) {
		this.firstInsdServDt = firstInsdServDt;
	}

	/**
	 * @return the practicePayTypcd
	 */
	public String getPracticePayTypcd() {
		return practicePayTypcd;
	}

	/**
	 * @param practicePayTypcd the practicePayTypcd to set
	 */
	public void setPracticePayTypcd(String practicePayTypcd) {
		this.practicePayTypcd = practicePayTypcd;
	}

	/**
	 * @return the createOprCd
	 */
	public String getCreateOprCd() {
		return createOprCd;
	}

	/**
	 * @param createOprCd the createOprCd to set
	 */
	public void setCreateOprCd(String createOprCd) {
		this.createOprCd = createOprCd;
	}

	/**
	 * @return the createTmestmp
	 */
	public Date getCreateTmestmp() {
		return createTmestmp;
	}

	/**
	 * @param createTmestmp the createTmestmp to set
	 */
	public void setCreateTmestmp(Timestamp createTmestmp) {
		this.createTmestmp = createTmestmp;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the replacedTmestmp
	 */
	public Date getReplacedTmestmp() {
		return replacedTmestmp;
	}

	/**
	 * @param replacedTmestmp the replacedTmestmp to set
	 */
	public void setReplacedTmestmp(Timestamp replacedTmestmp) {
		this.replacedTmestmp = replacedTmestmp;
	}

	/**
	 * @return the billingOptTypcd
	 */
	public String getBillingOptTypcd() {
		return billingOptTypcd;
	}

	/**
	 * @param billingOptTypcd the billingOptTypcd to set
	 */
	public void setBillingOptTypcd(String billingOptTypcd) {
		this.billingOptTypcd = billingOptTypcd;
	}

	/**
	 * @return the raSeqTypcd
	 */
	public String getRaSeqTypcd() {
		return raSeqTypcd;
	}

	/**
	 * @param raSeqTypcd the raSeqTypcd to set
	 */
	public void setRaSeqTypcd(String raSeqTypcd) {
		this.raSeqTypcd = raSeqTypcd;
	}

	/**
	 * @return the applicationDate
	 */
	public Date getApplicationDate() {
		return applicationDate;
	}

	/**
	 * @param applicationDate the applicationDate to set
	 */
	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * @return the groupTypcd
	 */
	public String getGroupTypcd() {
		return groupTypcd;
	}

	/**
	 * @param groupTypcd the groupTypcd to set
	 */
	public void setGroupTypcd(String groupTypcd) {
		this.groupTypcd = groupTypcd;
	}

	/**
	 * @return the occsNum
	 */
	public String getOccsNum() {
		return occsNum;
	}

	/**
	 * @param occsNum the occsNum to set
	 */
	public void setOccsNum(String occsNum) {
		this.occsNum = occsNum;
	}

	/**
	 * @return the raRecipTypcd
	 */
	public String getRaRecipTypcd() {
		return raRecipTypcd;
	}

	/**
	 * @param raRecipTypcd the raRecipTypcd to set
	 */
	public void setRaRecipTypcd(String raRecipTypcd) {
		this.raRecipTypcd = raRecipTypcd;
	}

	/**
	 * @return the rplctnUniqNum
	 */
	public BigDecimal getRplctnUniqNum() {
		return rplctnUniqNum;
	}

	/**
	 * @param rplctnUniqNum the rplctnUniqNum to set
	 */
	public void setRplctnUniqNum(BigDecimal rplctnUniqNum) {
		this.rplctnUniqNum = rplctnUniqNum;
	}

}


package ca.on.health.oocpa.api.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import ca.on.health.oocpa.data.entity.OOCPAReqEntity;
import ca.on.health.oocpa.data.entity.ReqWIPEntity;
import ca.on.health.oocpa.data.entity.StakeholderEntity;
import ca.on.health.oocpa.data.repository.OocpaReqRepository;
import ca.on.health.oocpa.data.repository.StakeholderRepository;
import ca.on.health.oocpa.model.ProviderModel;
import ca.on.health.oocpa.service.OocpaReqService;
import ca.on.health.oocpa.utililty.PdfGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.woo.htmltopdf.HtmlToPdf;
import io.woo.htmltopdf.HtmlToPdfObject;



@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/appreq")
public class OocpaAppReqcontroller {
    private Logger logger = LoggerFactory.getLogger(PatientVerifyController.class);

    @Autowired
    StakeholderRepository usersRepository;
    @Autowired OocpaReqService reqService;
	@PostMapping("/save")
	@ApiOperation(value = "Save OOCPA WIP Request", notes = "Save OOCPA WIP Request.", response = OOCPAReqEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
	 public ResponseEntity saveRequest(@RequestBody OOCPAReqEntity request) throws IOException, ClassNotFoundException {

        logger.debug("Recieved 'POST' Request on /api/save endpoint");
        ReqWIPEntity wipObj= new ReqWIPEntity();
        wipObj.setWipJsonData(request.toString().getBytes());
        ReqWIPEntity ReqWIPEntity = reqService.save(wipObj);

        Gson gson = new Gson();

		if(logger.isInfoEnabled())
			logger.info("OOCPA Request: {}",gson.toJson(ReqWIPEntity));

		return new ResponseEntity<>(gson.toJson(ReqWIPEntity), HttpStatus.OK);
	}
		@PostMapping("/submit")
		@ApiOperation(value = "Submit OOCPA Request", notes = "Submit OOCPA Request.", response = OOCPAReqEntity.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "OK"),
	            @ApiResponse(code = 401, message = "Unauthorized"),
	            @ApiResponse(code = 403, message = "Forbidden"),
	            @ApiResponse(code = 404, message = "Not Found"),
	            @ApiResponse(code = 500, message = "Internal Server Error")})	 
	public ResponseEntity submitRequest(@RequestBody OOCPAReqEntity request) throws IOException, ClassNotFoundException {
        logger.debug("Recieved 'POST' Request on /api/submit endpoint");
        OOCPAReqEntity ReqEntity= reqService.save(request);

        Gson gson = new Gson();

		if(logger.isInfoEnabled())
			logger.info("OOCPA Request: {}",gson.toJson(ReqEntity));

		return new ResponseEntity<>(gson.toJson(ReqEntity), HttpStatus.OK);

	}

	@GetMapping("/search/request")
	 public ResponseEntity searchRequest(String id) throws IOException, ClassNotFoundException {

       logger.debug("Recieved 'Get' Request on /search/request endpoint");
       OOCPAReqEntity reqEntity= reqService.searchRequest(id);
       Gson gson = new Gson();

		if(logger.isInfoEnabled())
			logger.info("OOCPA Request: {}",gson.toJson(reqEntity));
		

		return new ResponseEntity<>(gson.toJson(reqEntity), HttpStatus.OK);
	}
	
	@GetMapping("/search/request/pdf")
	public byte[] searchRequestPDF(String id) throws Exception
	{
	
		 logger.debug("Recieved 'Get' Request on /search/request/pdf endpoint");
	       OOCPAReqEntity reqEntity= reqService.searchRequest(id);
	       Gson gson = new Gson();

			if(logger.isInfoEnabled())
				logger.info("OOCPA Request: {}",gson.toJson(reqEntity));

			PdfGenerator pdf = new PdfGenerator();
			
	      return pdf.generatePDF(reqEntity);
	}
	@GetMapping("/search/wip")
	 public ResponseEntity searchWIPRequest(String id) throws IOException, ClassNotFoundException {
      logger.debug("Recieved 'get' Request on /search/wip endpoint");
      ReqWIPEntity reqEntity= reqService.searchWIPReq(id);

      Gson gson = new Gson();

		if(logger.isInfoEnabled())
			logger.info("OOCPA Request: {}",gson.toJson(reqEntity));
		
		

		return new ResponseEntity<>(gson.toJson(reqEntity), HttpStatus.OK);
	}
	
	
	
}

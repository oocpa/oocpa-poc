package ca.on.health.oocpa.data.repository;

import ca.on.health.oocpa.data.entity.OOCPAReqEntity;
import ca.on.health.oocpa.model.ProviderModel;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OocpaReqRepository extends CrudRepository<OOCPAReqEntity, Long> {
	@Query("SELECT r FROM  OOCPAReqEntity r WHERE r.id = ?1")
	public List<OOCPAReqEntity> findRequestEntity(Long id);

	public void save(List<ProviderModel> data);
}

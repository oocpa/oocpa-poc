package ca.on.health.oocpa.model;

public class ProviderModel {
	private String stakeholderNo;
	private String firstName;
	private String lastName;
	public String getStakeholderNo() {
		return stakeholderNo;
	}
	public void setStakeholderNo(String stakeholderNo) {
		this.stakeholderNo = stakeholderNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}

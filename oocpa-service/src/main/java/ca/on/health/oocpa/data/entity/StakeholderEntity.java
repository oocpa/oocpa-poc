package ca.on.health.oocpa.data.entity;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "STAKEHOLDER")
public class StakeholderEntity {
	@Id
	@Column(name="STAKEHOLDER_NUM")
	private String stakeholderNum;

	@Column(name="CREATE_OPR_CD")
	private String createOprCd;

	@Column(name="CREATE_TMESTMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTmestmp;

	@Column(name="STAKEHOLDER_TYPCD")
	private String stakeholderTypcd;

	@OneToMany(mappedBy="stakeholderNum")
	private Set<OrgCharcsEntity> orgCharcsCollection;

	@OneToMany(mappedBy="stakeholderNum")
	private Set<AgreementEntity> agreementCollection;
	
	/**
	 * @return the stakeholderNum
	 */
	public String getStakeholderNum() {
		return stakeholderNum;
	}

	/**
	 * @param stakeholderNum the stakeholderNum to set
	 */
	public void setStakeholderNum(String stakeholderNum) {
		this.stakeholderNum = stakeholderNum;
	}

	/**
	 * @return the createOprCd
	 */
	public String getCreateOprCd() {
		return createOprCd;
	}

	/**
	 * @param createOprCd the createOprCd to set
	 */
	public void setCreateOprCd(String createOprCd) {
		this.createOprCd = createOprCd;
	}

	/**
	 * @return the createTmestmp
	 */
	public Date getCreateTmestmp() {
		return createTmestmp;
	}

	/**
	 * @param createTmestmp the createTmestmp to set
	 */
	public void setCreateTmestmp(Timestamp createTmestmp) {
		this.createTmestmp = createTmestmp;
	}

	/**
	 * @return the stakeholderTypcd
	 */
	public String getStakeholderTypcd() {
		return stakeholderTypcd;
	}

	/**
	 * @param stakeholderTypcd the stakeholderTypcd to set
	 */
	public void setStakeholderTypcd(String stakeholderTypcd) {
		this.stakeholderTypcd = stakeholderTypcd;
	}

	/**
	 * @return the orgCharcsCollection
	 */
	public Set<OrgCharcsEntity> getOrgCharcsCollection() {
		return orgCharcsCollection;
	}

	/**
	 * @param orgCharcsCollection the orgCharcsCollection to set
	 */
	public void setOrgCharcsCollection(Set<OrgCharcsEntity> orgCharcsCollection) {
		this.orgCharcsCollection = orgCharcsCollection;
	}

	/**
	 * @return the agreementCollection
	 */
	public Set<AgreementEntity> getAgreementCollection() {
		return agreementCollection;
	}

	/**
	 * @param agreementCollection the agreementCollection to set
	 */
	public void setAgreementCollection(Set<AgreementEntity> agreementCollection) {
		this.agreementCollection = agreementCollection;
	}



	

}

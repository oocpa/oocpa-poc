package ca.on.health.oocpa.data.repository;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ca.on.health.oocpa.data.entity.StakeholderEntity;
import ca.on.health.oocpa.model.ProviderModel;

@Repository
public interface StakeholderRepository extends JpaRepository<StakeholderEntity, Long> {
	
	@Query("SELECT p FROM StakeholderEntity p WHERE p.stakeholderNum = ?1")
	public List<StakeholderEntity> findStakeholderBySN(String sn);

	public void save(List<ProviderModel> data);
}

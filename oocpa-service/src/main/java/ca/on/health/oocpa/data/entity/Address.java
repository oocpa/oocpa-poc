package ca.on.health.oocpa.data.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Address")
public class Address {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	
	   @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "created_timestamp", nullable = false)
	    private Date created;

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "last_update_timestamp", nullable = false)
	    private Date updated;

	    @PrePersist
	    protected void onCreate() {
	    	updated = created = new Date();
	    }

	    @PreUpdate
	    protected void onUpdate() {
	    	updated = new Date();
	    }
	    
		private String address_info;
	    private String city;
	    private String province;
	    private String postalCode;
	    
	    public String getAddress() {
			return address_info;
		}

		public void setAddress(String address) {
			this.address_info = address;
		}
		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}
		public String getProvince() {
			return province;
		}

		public void setProvince(String province) {
			this.province = province;
		}
		public String getPostalCode() {
			return postalCode;
		}

		public void setPostalCode(String postalCode) {
			this.postalCode = postalCode;
		}

}

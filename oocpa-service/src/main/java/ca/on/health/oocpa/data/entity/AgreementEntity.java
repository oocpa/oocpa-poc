package ca.on.health.oocpa.data.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "AgreementEntity")
@Table(name="@AGREEMENT")
public class AgreementEntity {

	@Id
	@Column(name="CLASSIFICATION_CD")
	private String classificationCd;

	@Column(name="AGREEMENT_TYPCD")
	private String agreementTypcd;

	@Column(name="CLAIM_SUB_NUM")
	private String claimSubNum;

	@Column(name="END_REASON_CD")
	private String endReasonCd;

	@Column(name="CREATE_TMESTMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTmestmp;

	@Column(name="CREATE_OPR_CD")
	private String createOprCd;

	@Column(name="EFFECTIVE_DATE")
	@Temporal(TemporalType.DATE)
	private Date effectiveDate;

	@Column(name="END_DATE")
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Column(name="REPLACED_TMESTMP")
	@Temporal(TemporalType.TIMESTAMP)
	private Date replacedTmestmp;

	@Column(name="RPLCTN_UNIQ_NUM")
	private BigDecimal rplctnUniqNum;

	@ManyToOne
	@JoinColumn(name="STAKEHOLDER_NUM")
	private StakeholderEntity stakeholderNum;

	@OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "CLAIM_SUB_NUM", insertable=false, updatable=false)
    private CsnCharcsEntity csnCharcs;
	
	/**
	 * Constructor for Agreement.
	 */
	public AgreementEntity() {
		super();
	}

	/**
	 * @return the classificationCd
	 */
	public String getClassificationCd() {
		return classificationCd;
	}

	/**
	 * @param classificationCd the classificationCd to set
	 */
	public void setClassificationCd(String classificationCd) {
		this.classificationCd = classificationCd;
	}

	/**
	 * @return the agreementTypcd
	 */
	public String getAgreementTypcd() {
		return agreementTypcd;
	}

	/**
	 * @param agreementTypcd the agreementTypcd to set
	 */
	public void setAgreementTypcd(String agreementTypcd) {
		this.agreementTypcd = agreementTypcd;
	}

	/**
	 * @return the claimSubNum
	 */
	public String getClaimSubNum() {
		return claimSubNum;
	}

	/**
	 * @param claimSubNum the claimSubNum to set
	 */
	public void setClaimSubNum(String claimSubNum) {
		this.claimSubNum = claimSubNum;
	}

	/**
	 * @return the endReasonCd
	 */
	public String getEndReasonCd() {
		return endReasonCd;
	}

	/**
	 * @param endReasonCd the endReasonCd to set
	 */
	public void setEndReasonCd(String endReasonCd) {
		this.endReasonCd = endReasonCd;
	}

	/**
	 * @return the createTmestmp
	 */
	public Date getCreateTmestmp() {
		return createTmestmp;
	}

	/**
	 * @param createTmestmp the createTmestmp to set
	 */
	public void setCreateTmestmp(Timestamp createTmestmp) {
		this.createTmestmp = createTmestmp;
	}

	/**
	 * @return the createOprCd
	 */
	public String getCreateOprCd() {
		return createOprCd;
	}

	/**
	 * @param createOprCd the createOprCd to set
	 */
	public void setCreateOprCd(String createOprCd) {
		this.createOprCd = createOprCd;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the replacedTmestmp
	 */
	public Date getReplacedTmestmp() {
		return replacedTmestmp;
	}

	/**
	 * @param replacedTmestmp the replacedTmestmp to set
	 */
	public void setReplacedTmestmp(Timestamp replacedTmestmp) {
		this.replacedTmestmp = replacedTmestmp;
	}

	/**
	 * @return the rplctnUniqNum
	 */
	public BigDecimal getRplctnUniqNum() {
		return rplctnUniqNum;
	}

	/**
	 * @param rplctnUniqNum the rplctnUniqNum to set
	 */
	public void setRplctnUniqNum(BigDecimal rplctnUniqNum) {
		this.rplctnUniqNum = rplctnUniqNum;
	}

	/**
	 * @return the stakeholderNum
	 */
	public StakeholderEntity getStakeholderNum() {
		return stakeholderNum;
	}

	/**
	 * @param stakeholderNum the stakeholderNum to set
	 */
	public void setStakeholderNum(StakeholderEntity stakeholderNum) {
		this.stakeholderNum = stakeholderNum;
	}

	/**
	 * @return the csnCharcs
	 */
	public CsnCharcsEntity getCsnCharcs() {
		return csnCharcs;
	}

	/**
	 * @param csnCharcs the csnCharcs to set
	 */
	public void setCsnCharcs(CsnCharcsEntity csnCharcs) {
		this.csnCharcs = csnCharcs;
	}


}

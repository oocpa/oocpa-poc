package ca.on.health.oocpa.api.controller;

import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;


import ca.on.health.oocpa.model.ProviderModel;
import ca.on.health.oocpa.service.ProviderVerifyService;



@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ProviderVerifyController {
	@Autowired
	ProviderVerifyService providerService;
    private Logger logger = LoggerFactory.getLogger(ProviderVerifyController.class);

	@GetMapping(value="/verify/provider",produces = "application/json")
	@CrossOrigin(origins = "*", allowCredentials = "true")
	public ResponseEntity isInGoodStanding(String sn){
	    logger.info("Recieved 'POST' Request on /verify/provider endpoint");

	    String result = providerService.performInGoodStanding(sn, "10");
	      Gson gson = new Gson();

			if(logger.isInfoEnabled())
				logger.info("OOCPA Request: {}",gson.toJson(result));

			return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
	}
	

}

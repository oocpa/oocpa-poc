package ca.on.health.oocpa.data.Dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ca.on.health.oocpa.data.entity.OOCPAReqEntity;
import ca.on.health.oocpa.data.repository.OocpaReqRepository;

@Repository
public class OocpaReqDAO {
	@Autowired
	private OocpaReqRepository reqRepository;
	
	private Logger logger = LoggerFactory.getLogger(OocpaReqDAO.class);

	public OOCPAReqEntity saveOrUpdate(OOCPAReqEntity req) {
		return reqRepository.save(req);
	}
	
	public OOCPAReqEntity findById(String id) {
		List<OOCPAReqEntity> requests=reqRepository.findRequestEntity(Long.parseLong(id));
		
		if( requests.isEmpty()) {
			return null;
		} else {
			return requests.get(0);
		}
	}
}

package ca.on.health.oocpa.api.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import ca.on.health.oocpa.model.PatientModel;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PatientVerifyController {
    private Logger logger = LoggerFactory.getLogger(PatientVerifyController.class);

	@PostMapping("/verify/patient")
	 public ResponseEntity verifyPatient(@RequestBody PatientModel patient) throws IOException, ClassNotFoundException {

        logger.debug("Recieved 'POST' Request on /verification endpoint");
        
        Gson gson = new Gson();

        //
        // CONSTRUCT RESPONSE HEADER
        //
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        String body = gson.toJson("result");

        return ResponseEntity.status(HttpStatus.OK).headers(headers).body(body);


	}
	
	@GetMapping("/verify/patient/ping")
	 public String verifyPatient() throws IOException, ClassNotFoundException {

       logger.info("Recieved 'POST' Request on /verification endpoint");
       
       return "OK!";


	}

}

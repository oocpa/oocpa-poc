package ca.on.health.oocpa.utililty;

import ca.on.health.oocpa.data.entity.StakeholderEntity;
import ca.on.health.oocpa.common.Constant;

public class ProviderValidationUtil {
	/**
	 * This method checks to see if the stakeholder specified is an origanization
	 * @param stakeholder
	 * @return
	 * @throws SystemException
	 */
	public static boolean isOrganization(StakeholderEntity stakeholder) {
		boolean bOrganization = false;
		
		String stakeholderTypcd = stakeholder.getStakeholderTypcd();		
		if (stakeholderTypcd.equalsIgnoreCase("OR")){
			bOrganization = true;
		}
		
		return bOrganization;
	}
	
	/**
	 * This method checks to see if the stakeholder specified is an person.
	 * @param stakeholder
	 * @return
	 * @throws SystemException
	 */
	public static boolean isPerson(StakeholderEntity stakeholder) {
		boolean bPerson = false;
		
		String stakeholderTypcd = stakeholder.getStakeholderTypcd();		
		if (stakeholderTypcd.equalsIgnoreCase("PR")){
			bPerson = true;
		}
		
		return bPerson;
	}
}

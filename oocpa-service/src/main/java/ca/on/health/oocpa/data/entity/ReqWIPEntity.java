package ca.on.health.oocpa.data.entity;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="OOCPA_REQUEST_WIP")
public class ReqWIPEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Lob
    private byte[] wipJsonData;
	
	public byte[] getWipJsonData() {
		return wipJsonData;
	}

	public void setWipJsonData(byte[] wipJsonData) {
		this.wipJsonData = wipJsonData;
	}
	
	public Long geId() {
		return this.id;
	}

}

package ca.on.health.oocpa.utililty;

import java.io.IOException;
import java.io.InputStream;

import java.text.SimpleDateFormat;
import java.util.Date;

import ca.on.health.oocpa.data.entity.OOCPAReqEntity;
import io.woo.htmltopdf.HtmlToPdf;
import io.woo.htmltopdf.HtmlToPdfObject;


public class PdfGenerator {
	
	
	public PdfGenerator() {
		
	}
	
	private String generateHTML(OOCPAReqEntity reqEntity)
	{
		String template = "<html>\r\n" + 
				"    <body>\r\n" + 
				"\r\n" + 
				"        <h2 style='text-align:center'>OOCPA Summary</h2>\r\n" + 
				"        <h2>OOCPA Request</h2>\r\n" + 
				"        <div style='border:'>\r\n" + 
				"            <h3>Section 1 - Physcian Information</h3>\r\n" + 
				"            <div style='margin-bottom:0;'><strong>Name</strong></div>\r\n" + 
				"            <div>\r\n" + 
				"             <span style='padding-right:5px;margin-right:10px;border-right:1px solid black;'>\r\n" + 
				"                    First name: \r\n" + 
				"                    " + reqEntity.getPhysician().getFirstName() +              
				"                </span>\r\n" + 
				"                <span> \r\n" + 
				"                    Family:" +  reqEntity.getPhysician().getLastName() +  
				"                </span>\r\n" + 
				"            </div>\r\n" + 
				"            <div style='margin-bottom:0;'><strong>Billing Number</strong></div>\r\n" + 
				"            <div>\r\n" + 
				"                <span>\r\n" + 
									reqEntity.getPhysician().getBillingNumber() +  
				"                </span>\r\n" + 
				"            </div>\r\n" + 
				"            <div style='margin-bottom:0;'><strong>Email</strong></div>\r\n" + 
				"            <div>\r\n" + 
									reqEntity.getPhysician().getEmail() +  
				"            </div>\r\n" +  
				"        </div>\r\n" + 
				"\r\n" + 
				"    </body>\r\n" + 
				"</html>";
		
		return template;
	}
	
	public byte[] generatePDF(OOCPAReqEntity reqEntity) throws IOException
	{
	
		String fileNameWDate = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".pdf";
		HtmlToPdf pdf = HtmlToPdf.create()
			    .object(HtmlToPdfObject.forHtml(generateHTML(reqEntity)));
		
		InputStream pdfGen = pdf.convert();
	    byte[] pdfGener = pdfGen.readAllBytes();

			
		
		return pdfGener;
		
	}
	
	

}

package ca.on.health.oocpa.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.on.health.oocpa.data.Dao.StakeholderDao;
import ca.on.health.oocpa.data.entity.StakeholderEntity;
import ca.on.health.oocpa.data.entity.CsnCharcsEntity;
import ca.on.health.oocpa.utililty.ProviderValidationUtil;

@Service
public class ProviderVerifyService {
	
	@Autowired
	private StakeholderDao stakeholderDao;

    private Logger logger = LoggerFactory.getLogger(ProviderVerifyService.class);

	/**
	 * Specifies that the Stakeholder passed authorization.
	 */
	public static final String VALID = "IHCAU0001";

	/**
	 * Specifies that the Stakeholder did not pass authorization.
	 */
	public static final String INVALID_NOT_AUTHORIZED = "EHCAU0016";

	/**
	 * Specifies that the Eaccess ID is not enrolled.
	 */
	public static final String INVALID_IDP_NOT_ENROLLED = "EHCAU0017";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Stakeholder was identified as
	 * an Organization and they had did not have an Open Status.
	 */
	public static final String INVALID_SR_ORG_NOT_OPEN = "EHCAU0004";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Stakeholder is not known to
	 * MOHLTC.
	 */
	public static final String INVALID_SR_SN = "EHCAU0018";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Stakeholder is not enrolled to
	 * use the HCV Web Service.
	 */
	public static final String INVALID_NOT_ENROLLED = "EHCAU0019";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Stakeholder use of the HCV
	 * Web Service is currently under suspension.
	 */
	public static final String INVALID_SUSPENDED = "EHCAU0020";
	/**
	 * Specifies that the Stakeholder did not pass authorization.  The vendor is unknown
	 */
	public static final String INVALID_VENDOR = "EHCAU0021";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Stakeholder was identified as
	 * an Organization and they had did not have an Open Status.
	 */
	public static final String INVALID_SU_ORG_NOT_OPEN = "EHCAU0022";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Stakeholder is not known to
	 * MOHLTC.
	 */
	public static final String INVALID_SU_SN = "EHCAU0023";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Stakeholder was identified as
	 * a Person and they had an invalid Claims Submission Status.
	 */
	public static final String INVALID_SU_PERSON_CSN_STATUS = "EHCAU0011";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Stakeholder was identified as
	 * an Organization and they had an invalid Claims Submission Status.
	 */
	public static final String INVALID_SU_ORG_CSN_STATUS = "EHCAU0012";

	/**
	 * Specifies that the Stakeholder did not pass authorization.  The Identity Provider is unknown
	 */
	public static final String INVALID_IDP = "EHCAU0013";
	
	/**
	 * Specifies that the Service User does not have an eaccess id
	 */
	public static final String SERVICE_USER_UNKNOWN_TO_EACCESS = "EHCAU0027";
	
	/**
	 * Specifies that the Service User is not registered as an 'SU'
	 */
	public static final String SERVICE_USER_NOT_REGISTERED_AS_SU = "EHCAU0028";
	
	/**
	 * Specifies that the Service User and User Relationship not found
	 */
	public static final String SERVICE_USER_AND_USER_HAVE_NO_RELATIONSHIP = "EHCAU0029";

	enum OrgReturnCodes { TRUE, INVALID_ORG_STATUS, ORG_NOT_OPEN };

	private final String CLAIM_SUB_STAT_CD_11 = "11";
	private final String CLAIM_SUB_STAT_CD_12 = "12";
	private final String CLAIM_SUB_STAT_CD_21 = "21";
	private final String CLAIM_SUB_STAT_CD_41 = "41";
	/**
	 * This method performs the necessary checks to ensure the
	 * an SN or CSN is in good standing.
	 * 
	 * @param stakeholderNum - for the Stakeholder.
	 * @return The outcome of the authorization.
	 * @throws SystemException
	 */
	public String performInGoodStanding(String stakeholderNum, String service) {
		
		String result = null;
		if ((stakeholderNum!=null) && stakeholderNum.length()>0){
			StakeholderEntity stakeholder = this.stakeholderDao.findStakeholder(stakeholderNum);
			if (stakeholder != null){

				if (ProviderValidationUtil.isOrganization(stakeholder)) {
					OrgReturnCodes rc = isOrganizationInGoodStanding(stakeholder);

					if (rc == OrgReturnCodes.TRUE) {
						result = VALID;
					} else if (rc == OrgReturnCodes.ORG_NOT_OPEN) {
						result = INVALID_SR_ORG_NOT_OPEN;
					} else {
						result = INVALID_NOT_AUTHORIZED;
					}
				} else if (ProviderValidationUtil.isPerson(stakeholder)) {
					if (this.isPersoninGoodStanding(stakeholder)) {
						result = VALID;
					} else {
						result = INVALID_NOT_AUTHORIZED;
					}
				} else {
					result = INVALID_SR_SN;
				}
			} else {
				result = INVALID_SR_SN;
			}
		} else {
			result = INVALID_SR_SN;
		}
		logger.info(this.getClass().getName(), "performInGoodStanding(): Outcome: " + result);
		return result;
	}
	/**
	 * This method checks to see if a specified organization is in good standing.
	 * @param stakeholder
	 * @return
	 * @throws SystemException
	 */
	public OrgReturnCodes isOrganizationInGoodStanding(StakeholderEntity stakeholder) {
		OrgReturnCodes eInGoodStanding = OrgReturnCodes.INVALID_ORG_STATUS;
		
		String stakeholderTypcd = stakeholder.getStakeholderTypcd();		
		if (stakeholderTypcd.equalsIgnoreCase("OR")){					
			List<CsnCharcsEntity> csnCharcsList = stakeholderDao.findCSNsBySN(stakeholder);				
			if (csnCharcsList != null && csnCharcsList.size() > 0){
				for (CsnCharcsEntity csnCharcs : csnCharcsList){			
					String claimSubStatCd = csnCharcs.getClaimSubStatCd();
					if (claimSubStatCd.equalsIgnoreCase(CLAIM_SUB_STAT_CD_11)){
						eInGoodStanding = OrgReturnCodes.TRUE;
						break; 						
					}
				}
			} else if (stakeholderDao.isOrgCharcsResultFound(stakeholder)){
				eInGoodStanding = OrgReturnCodes.TRUE;
			} else {
				eInGoodStanding = OrgReturnCodes.ORG_NOT_OPEN;
			}
		} 
		
		return eInGoodStanding;
	}
	
	/**
	 * This method checks to see if a specified person is in good standing
	 * @param stakeholder
	 * @return
	 * @throws SystemException
	 */
	public boolean isPersoninGoodStanding(StakeholderEntity stakeholder) {
		boolean bInGoodStanding = false;
		
		String stakeholderTypcd = stakeholder.getStakeholderTypcd();		
		if (stakeholderTypcd.equalsIgnoreCase("PR")){
			List<CsnCharcsEntity> csnCharcsList = stakeholderDao.findCSNsBySN(stakeholder);				
			if (csnCharcsList != null && csnCharcsList.size() > 0){				
				for (CsnCharcsEntity csnCharcs : csnCharcsList){
					String claimSubStatCd = csnCharcs.getClaimSubStatCd();
					if (claimSubStatCd.equalsIgnoreCase(CLAIM_SUB_STAT_CD_11)
							|| claimSubStatCd.equalsIgnoreCase(CLAIM_SUB_STAT_CD_12)
							|| claimSubStatCd.equalsIgnoreCase(CLAIM_SUB_STAT_CD_21)
							|| claimSubStatCd.equalsIgnoreCase(CLAIM_SUB_STAT_CD_41)) {							
						bInGoodStanding = true;
						break;
					}
				}
			} 
		}

		return bInGoodStanding;
	}


}
